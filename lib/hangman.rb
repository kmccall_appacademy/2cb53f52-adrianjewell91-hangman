#My goals for this project
# => 1. Methods of no more than two lines
# Abstraction between public and private methods.
# Refactor my algorithms hard code everytime I make one.
#Walk through my code after I write it.

#Version Two - Individual Puts the Word and Computer guesses intelligently.
#The computer should first create a list of only the words that have the same length
#as the secret word.
#Then: after it guesses a letter, it should one of two things:
  # => 1. Delete all words from that list that have the letter
          #If the letter was not in the word.
  # => 2. Scale down the dictionary to word that share
        #letters in the same position as the secret word.
        
require 'byebug'

class Hangman
  def initialize(players = {guesser: ComputerPlayer.new,
                            referee: HumanPlayer.new}  )
    @guesser = players[:guesser]
    @referee = players[:referee]
  end

  attr_reader :guesser, :referee, :board

  def setup
    tell_guesser
    set_board
  end

  def take_turn
    until complete?
      @guess = get_guess
      update_board(get_indices(@guess),@guess)
      give_feedback(get_indices(@guess),@guess)
    end
  end

  private

  def give_feedback(i,l)
    @guesser.handle_response(i,l)
  end

  def result; @board; end

  def complete?; @referee.is_the_word?(@board); end

  def get_guess
    @guesser.get_guess
  end

  def get_indices(guess);
    @referee.check_guess(guess);
  end

  def update_board(indices,letter)
    indices.each{|i| @board[i] = letter}
    puts @board
  end

  def tell_guesser
    @guesser.register_secret_length(@referee.secret_word_length)
  end

  def set_board
    @board = Array.new(@referee.secret_word_length,"_").join()
  end
end

################################################################################
class HumanPlayer
  def initialize; @secret_word=get_word end

  def secret_word_length
    @secret_word.length
  end

  def register_secret_length(length)
    @length
  end

  def get_guess
    puts "Pick a letter:"
    gets.chomp
  end

  def check_guess(l)
    @secret_word.include?(l) ? find_idx(l) : []
  end

  def handle_response(i,l)
  end

  def is_the_word?(word)
    word == @secret_word
  end

  private

  def get_word
    puts "Pick a word: "
    gets.chomp
  end

  def find_idx(l)
    (0..secret_word_length-1).reduce([]) do |acc,el|
      @secret_word[el] == l ? acc << el : acc
      acc
    end
  end

end

################################################################################

class ComputerPlayer
  def initialize(dictionary = File.open("lib/dictionary.txt"));
    @secret_word = dictionary.readlines.sample.chomp
    dictionary.close
  end

  def register_secret_length(length)
    generate_database(length)
  end

  def secret_word_length
    @secret_word.length
  end

  def check_guess(l)
    @secret_word.include?(l) ? find_idx(l) : []
  end

  def get_guess
    generate_letter
  end

  def handle_response(indices,letter) #filters candidate words
    case indices.size > 0
    when true
      @candidate_words = select_valid_words(@candidate_words,indices,letter)
    else
      @candidate_words = delete_invalid_words(@candidate_words, letter)
    end
  end

  def is_the_word?(word)
    word == @secret_word
  end

  private

  def generate_letter
    @candidate_words.join.split('').uniq.sample
  end

  def find_idx(l) #Good helper method.
    (0..secret_word_length-1).reduce([]) do |acc,el|
      @secret_word[el] == l ? acc << el : acc
      acc
    end
  end

  def generate_database(length)
   f = File.open("lib/dictionary.txt")
   @candidate_words = f.readlines.select do |el|
      el.chomp.length == length; end.map{|el| el.chomp}
  end

  def select_valid_words(words,idx,l)
    idx.map!{|i| words.select{|el| el[i].include?(l)}}.flatten.uniq
  end

  def delete_invalid_words(words,l)
    words.reject{|el| el.include?(l)}
  end

end
